#!/usr/bin/make -f
#export DH_VERBOSE=1

DPATH := $(abspath $(dir $(MAKEFILE_LIST)))
DTYPE := +dfsg
PKG := libgetopt-java
VER ?= $(shell dpkg-parsechangelog -l$(DPATH)/changelog | perl -ne 'print $$1 if m{Version:\s*(\d[\d\.]+)}')

export JAVA_HOME=/usr/lib/jvm/default-java
export JAR=$(JAVA_HOME)/bin/jar
export JAVAC=$(JAVA_HOME)/bin/javac
export CLASSPATH=$(JAVA_HOME)/jre/lib/rt.jar:.

%:
	dh $@ --with javahelper --with jh_maven_repo_helper

override_dh_auto_build:
	[ -f $(CURDIR)/GetoptDemo.java ] || mv $(CURDIR)/gnu/getopt/GetoptDemo.java .
	mkdir -p $(CURDIR)/classes
	${JAVAC} -target 1.8 -source 1.8 -d classes -classpath ${CLASSPATH} gnu/getopt/*.java
	cp $(CURDIR)/gnu/getopt/*.properties $(CURDIR)/classes/gnu/getopt
	cd $(CURDIR)/classes; ${JAR} cvf ../gnu-getopt.jar gnu

override_dh_installexamples:
	dh_installexamples GetoptDemo.java

override_dh_installchangelogs:
	dh_installchangelogs gnu/getopt/ChangeLog

override_dh_clean:
	dh_clean
	$(RM) -r $(CURDIR)/classes $(CURDIR)/api
	[ -f $(CURDIR)/gnu/getopt/GetoptDemo.java ] || mv $(CURDIR)/GetoptDemo.java $(CURDIR)/gnu/getopt/

.PHONY: get-orig-source
get-orig-source: $(PKG)_$(VER)$(DTYPE).orig.tar.gz
	@

$(PKG)_$(VER)$(DTYPE).orig.tar.gz:
	@echo "# Downloading..."
	uscan --noconf --verbose --rename --destdir=$(CURDIR) --check-dirname-level=0 --force-download --download-version $(VER) $(DPATH)
	@echo "# Extracting..."
	mkdir $(PKG)-$(VER) \
	&& tar xf $(PKG)_$(VER).orig.tar.* --directory $(PKG)-$(VER) --strip-components 0 \
	|| $(RM) -r $(PKG)-$(VER)
	@echo "# Clean-up..."
	cd $(PKG)-$(VER) \
	&& $(RM) -r -v \
		gnu/getopt/*.html \
		gnu/getopt/*.class
	@echo "# Packing..."
	tar -caf "$(PKG)_$(VER)$(DTYPE).orig.tar.gz" "$(PKG)-$(VER)" \
	&& $(RM) -r "$(PKG)-$(VER)"
